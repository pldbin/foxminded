package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func (bh *BotHandler) askTime(chatID int64) {
	markup := tgbotapi.NewReplyKeyboard(tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("10:00"),
		tgbotapi.NewKeyboardButton("12:00"),
		tgbotapi.NewKeyboardButton("14:00"),
	))
	msg := tgbotapi.NewMessage(chatID, "Please, select the time.")
	msg.ReplyMarkup = markup
	_, err := bh.Bot.Send(msg)

	if err != nil {
		log.Panic(err)
	}
}
