package subscriptions

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// DeleteUserIfUnsubscribed deletes a user from the collection if they are unsubscribed.
func (m *Model) DeleteUserIfUnsubscribed(chatID int64) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	filter := bson.M{"chat_id": chatID, "subscribed": false}
	_, err := m.Collection.DeleteOne(ctx, filter)

	return err
}
