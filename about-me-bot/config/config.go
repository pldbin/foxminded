package config

import (
	"github.com/caarlos0/env"
)

type Config struct {
	Btoken               string `env:"BTOKEN" envDefault:"5844982587:AAFG3r6a7SIi11mrjFYqA_QbgrMacqdPayI"`
	WeatherAPI           string `env:"WEATHER" envDefault:"be7bb507c2611c82815204ef4119745f"`
	OpenWeatherMapAPIURL string `env:"OPEN_WEATHER_MAP_API_URL" envDefault:"https://api.openweathermap.org"`
	MongoDBURI           string `env:"MONGODB_URI" envDefault:"mongodb://localhost:27017"`

	ServiceName string `env:"SERVICE_NAME" envDefault:"about-me-bot"`
	LogServer   string `env:"LOG_SERVER" envDefault:"stdout"`
	LogLevel    string `env:"LOG_LEVEL" envDefault:"info"`
}

// NewConfig parses envs and constructs the config
func NewConfig() (*Config, error) {
	var config Config

	if err := env.Parse(&config); err != nil {
		return nil, err
	}

	return &config, nil
}
