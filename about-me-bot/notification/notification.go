// notification.go
package notification

import (
	"About_me_Bot/bot"
	"About_me_Bot/logger"
	"About_me_Bot/subscriptions"
	"About_me_Bot/weather"
	"fmt"
	"log"
	"time"
)

// NotifyUsers gets a list of users to whom the notification needs to be sent, and for each user calls the function SendWeatherForecast.
func NotifyUsers(bh *bot.BotHandler, mdl *subscriptions.Model, lgr *logger.LogrusLogger, t time.Time) error {
	users, err := mdl.GetUsersToNotify(t.UTC().Hour())
	if err != nil {
		log.Printf("failed read users for weather forecast")
		return err
	}
	fmt.Println(users)
	// проходжусь по користувачах і відправляю їм погоду
	var lastError error
	for _, user := range users {
		err := SendWeatherForecast(bh, lgr, user)
		if err != nil {
			log.Printf("Failed to send weather forecast to user: %v", err)
			lastError = err
			continue
		}
	}
	return lastError
}

// SendWeatherForecast is a function that sends a weather forecast message to a user.
func SendWeatherForecast(bh *bot.BotHandler, lgr *logger.LogrusLogger, user subscriptions.Item) error {
	weather, err := weather.GetWeather(bh.Config.OpenWeatherMapAPIURL, user.Latitude, user.Longitude, bh.Config.WeatherAPI)
	if err != nil {
		log.Printf("Failed to get weather for user: %v", err)
		return err
	}

	message := fmt.Sprintf("Weather for you: %s\nLongitude %f, Latitude %f",
		weather, user.Longitude, user.Latitude)
	err = bh.SendMessage(user.ChatID, message)
	if err != nil {
		log.Printf("Failed to send message to user: %v", err)
		return err
	}
	return nil
}
