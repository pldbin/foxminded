package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

// unsubscriber deletes a user from the database if they have unsubscribed and sends an unsubscribe message to the user.
func (bh *BotHandler) unsubscriber(chatID int64) {
	err := bh.Model.DeleteUserIfUnsubscribed(chatID)
	if err != nil {
		bh.Logger.Errorf("Error while attempting to delete user: %v", err)
		return
	}
	msg := tgbotapi.NewMessage(chatID, "You have successfully unsubscribed and deleted!")
	_, err = bh.Bot.Send(msg)
	if err != nil {
		bh.Logger.Errorf("Error while attempting to send unsubscribe and delete message: %v", err)
	}
}
