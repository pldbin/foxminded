package cache

import (
	"fmt"
	"os"

	"3-rest-api/internal/seelog"
	"github.com/go-redis/redis/v7"
)

var client *redis.Client

func Init() (*redis.Client, error) {
	host := os.Getenv("REDIS_HOST")
	if host == "" {
		host = "redis"
	}
	port := os.Getenv("REDIS_PORT")
	if port == "" {
		port = "6379"
	}

	client = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", host, port),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})

	_, err := client.Ping().Result()
	if err != nil {
		seelog.SeeLog.Errorf("failed to connect to redis: %v", err)
		return nil, err
	}

	return client, nil
}

func GetClient() *redis.Client {
	return client
}
