package handlers_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"3-rest-api/internal/handlers"
	"3-rest-api/internal/models"
	"3-rest-api/internal/repository"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestUserHandler_CreateUserHandler(t *testing.T) {
	createUserHandlerTestCases := []struct {
		name               string
		user               models.User
		mockRepoError      error
		expectedStatusCode int
		expectedError      string
	}{
		{name: "Valid User", user: models.User{Role: "user", Email: "test@test.com", Password: "validpassword1234", FirstName: "Test", LastName: "User"}, mockRepoError: nil, expectedStatusCode: http.StatusInternalServerError, expectedError: ""},
		{name: "Empty Email Error", user: models.User{Email: "", Password: "validpassword1234", FirstName: "Test", LastName: "User"}, mockRepoError: nil, expectedStatusCode: http.StatusBadRequest, expectedError: "email is required"},                                               // changed "Email is required" to "email is required"
		{name: "Short Password Error", user: models.User{Email: "test@test.com", Password: "short", FirstName: "Test", LastName: "User"}, mockRepoError: nil, expectedStatusCode: http.StatusBadRequest, expectedError: "password must be between 8 and 32 characters"},                // changed "Password must be between 8 and 32 characters" to "password must be between 8 and 32 characters"
		{name: "Long Password Error", user: models.User{Email: "test@test.com", Password: strings.Repeat("a", 33), FirstName: "Test", LastName: "User"}, mockRepoError: nil, expectedStatusCode: http.StatusBadRequest, expectedError: "password must be between 8 and 32 characters"}, // changed "Password must be between 8 and 32 characters" to "password must be between 8 and 32 characters"
		{name: "Empty First Name Error", user: models.User{Email: "test@test.com", Password: "validpassword1234", FirstName: "", LastName: "User"}, mockRepoError: nil, expectedStatusCode: http.StatusBadRequest, expectedError: "FirstName is required"},
		{name: "Empty Last Name Error", user: models.User{Email: "test@test.com", Password: "validpassword1234", FirstName: "Test", LastName: ""}, mockRepoError: nil, expectedStatusCode: http.StatusBadRequest, expectedError: "LastName is required"},
		{name: "Repo Error", user: models.User{Role: "user", Email: "test@test.com", Password: "validpassword1234", FirstName: "Test", LastName: "User"}, mockRepoError: errors.New("some repository error"), expectedStatusCode: http.StatusInternalServerError, expectedError: "error creating user\n"},
	}

	for _, tt := range createUserHandlerTestCases {
		t.Run(tt.name, func(t *testing.T) {

			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockRepo := repository.NewMockUserRepository(ctrl)

			mockRepo.EXPECT().CreateUser(&tt.user).Return(tt.mockRepoError).AnyTimes()

			handler := handlers.UserHandler{Repo: mockRepo}

			requestBody, err := json.Marshal(tt.user)
			require.NoError(t, err)

			request := httptest.NewRequest(http.MethodPost, "/user", bytes.NewBuffer(requestBody))
			responseRecorder := httptest.NewRecorder()

			handler.CreateUserHandler(responseRecorder, request)

			assert.Equal(t, tt.expectedStatusCode, responseRecorder.Code)

			if tt.expectedError != "" {
				body, err := io.ReadAll(responseRecorder.Body)
				require.NoError(t, err)
				assert.Contains(t, string(body), tt.expectedError)
			}
		})
	}
}

func TestUserHandler_UpdateUserHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockUserRepository(ctrl)

	user := models.User{ID: 1, Email: "test@test.com", FirstName: "Test", LastName: "User"}
	mockRepo.EXPECT().UpdateUser(&user).Return(nil)

	handler := handlers.UserHandler{Repo: mockRepo}

	requestBody, err := json.Marshal(user)
	if err != nil {
		t.Errorf("Failed to marshal user: %v", err)
	}

	request := httptest.NewRequest(http.MethodPut, "/user/"+fmt.Sprint(user.ID), bytes.NewBuffer(requestBody))
	request = mux.SetURLVars(request, map[string]string{"id": fmt.Sprint(user.ID)})
	responseRecorder := httptest.NewRecorder()

	handler.UpdateUserHandler(responseRecorder, request)

	assert.Equal(t, http.StatusOK, responseRecorder.Code)
}

func TestUserHandler_DeleteUserHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockUserRepository(ctrl)

	user := &models.User{ID: 1}
	mockRepo.EXPECT().DeleteUser(user).Return(nil)

	handler := handlers.UserHandler{Repo: mockRepo}

	request := httptest.NewRequest(http.MethodDelete, "/user/"+fmt.Sprint(user.ID), nil)
	request = mux.SetURLVars(request, map[string]string{"id": fmt.Sprint(user.ID)})
	responseRecorder := httptest.NewRecorder()

	handler.DeleteUserHandler(responseRecorder, request)

	assert.Equal(t, http.StatusOK, responseRecorder.Code)
}

func TestUserHandler_GetUsersHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := repository.NewMockUserRepository(ctrl)
	mockRepo.EXPECT().GetUsers(1, 10).Return([]models.User{{Email: "test@test.com", FirstName: "Test", LastName: "User"}}, nil)

	handler := handlers.UserHandler{Repo: mockRepo}

	request := httptest.NewRequest(http.MethodGet, "/users?page=1&pageSize=10", nil)
	responseRecorder := httptest.NewRecorder()

	handler.GetUsersHandler(responseRecorder, request)

	assert.Equal(t, http.StatusOK, responseRecorder.Code)
	assert.Contains(t, responseRecorder.Body.String(), "test@test.com")
}

func TestUserHandler_GetUserHandler(t *testing.T) {
	t.Run("valid user id", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockRepo := repository.NewMockUserRepository(ctrl)
		expectedUser := &models.User{Email: "test@test.com", FirstName: "Test", LastName: "User"}

		mockRepo.EXPECT().GetUser("1").Return(expectedUser, nil)

		handler := handlers.UserHandler{
			Repo: mockRepo,
		}

		request := httptest.NewRequest(http.MethodGet, "/user/", nil)
		request = mux.SetURLVars(request, map[string]string{"id": "1"})
		responseRecorder := httptest.NewRecorder()

		handler.GetUserHandler(responseRecorder, request)

		assert.Equal(t, http.StatusOK, responseRecorder.Code)

		receivedUser := &models.User{}
		err := json.NewDecoder(responseRecorder.Body).Decode(receivedUser)

		if err != nil {
			t.Fatalf("could not decode response: %v", err)
		}

		assert.Equal(t, expectedUser.Email, receivedUser.Email)
		assert.Equal(t, expectedUser.FirstName, receivedUser.FirstName)
		assert.Equal(t, expectedUser.LastName, receivedUser.LastName)
	})

	t.Run("user not found", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		mockRepo := repository.NewMockUserRepository(ctrl)
		mockRepo.EXPECT().GetUser("1").Return(nil, errors.New("sql: no rows in result set"))

		handler := handlers.UserHandler{
			Repo: mockRepo,
		}

		request := httptest.NewRequest(http.MethodGet, "/user/", nil)
		request = mux.SetURLVars(request, map[string]string{"id": "1"})
		responseRecorder := httptest.NewRecorder()

		handler.GetUserHandler(responseRecorder, request)

		assert.Equal(t, http.StatusNotFound, responseRecorder.Code)

		response := struct {
			Error string `json:"error"`
		}{}
		if err := json.NewDecoder(responseRecorder.Body).Decode(&response); err != nil {
			t.Fatalf("could not decode response: %v", err)
		}

		assert.Equal(t, "User not found", response.Error)
	})
}
