package bot

func (bh *BotHandler) exit(chatID int64) {
	err := bh.SendMessage(chatID, "Bye!")
	if err != nil {
		bh.Logger.Errorf("Error: '%v'\n", err)
	}
}
