package middlewares

import (
	"net/http"

	"github.com/golang-jwt/jwt"
	"github.com/gorilla/mux"
)

type CustomClaims struct {
	Role string `json:"role"`
	jwt.StandardClaims
}

// RoleBasedAccessControlHandler middleware for role-based access control
func RoleBasedAccessControlHandler() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			// Read the 'Authorization' header from the request
			tokenHeader := r.Header.Get("Authorization")

			// Check if the header is present and in correct form, send error message if not
			if tokenHeader == "" {
				// If a token is missing, an unauthorized error message is sent
				http.Error(w, "Missing auth token", http.StatusUnauthorized)
				return
			}

			// Parse and check the token
			token, err := jwt.ParseWithClaims(tokenHeader, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
				return []byte("YourJWTSecret"), nil
			})

			// Return 401 status if there's an error when parsing or validating the token
			if err != nil || !token.Valid {
				http.Error(w, "Error parsing or invalid auth token", http.StatusUnauthorized)
				return
			}

			// Get the claims from the token
			claims := token.Claims.(*CustomClaims)

			// For PUT or DELETE methods, role has to be "admin"
			if r.Method == http.MethodPut || r.Method == http.MethodDelete {
				if claims.Role != "admin" {
					http.Error(w, "Access denied: admin role required for this operation", http.StatusUnauthorized)
					return
				}
			} else {
				if claims.Role != "admin" && claims.Role != "user" {
					http.Error(w, "Access denied: invalid user role", http.StatusUnauthorized)
					return
				}
			}

			next.ServeHTTP(w, r)
		})
	}
}
