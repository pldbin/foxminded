package repository

import (
	"database/sql"
	"errors"
	"time"

	"3-rest-api/internal/models"
	"3-rest-api/internal/seelog"
)

// UserRepository represents an interface for managing user data in a repository.
type UserRepository interface {
	CreateUser(user *models.User) error
	GetUser(id string) (*models.User, error)
	UpdateUser(id string, user *models.User) error
	GetUsers(page, pageSize int) ([]models.User, error)
	DeleteUser(user *models.User) error
	CastVote(userID, profileID, vote int) error
	GetVotes(userID int) ([]models.Vote, error)
	DeleteVote(userID, profileID int) error
	UpdateVote(userID, profileID, vote int) error
	GetUserByEmail(email string) (*models.User, error)
}

func NewUserRepository(Db *sql.DB) UserRepository {
	return &UserRepositoryPostgres{Db: Db}
}

// UserRepositoryPostgres represents a repository implementation for managing users in a PostgresSQL database.
type UserRepositoryPostgres struct {
	Db *sql.DB
}

// CreateUser is a method that creates a new user in the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) CreateUser(user *models.User) error {
	hashedPassword, err := BcryptHashPassword(user.Password)
	if err != nil {
		err := seelog.SeeLog.Errorf("Error hashing password: %v", err.Error())
		if err != nil {
			return err
		}
		return err
	}
	user.HashedPassword = hashedPassword

	query := `INSERT INTO users (email, hashed_password, first_name, last_name, role, created_at, updated_at) 
			  VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`

	err = repo.Db.QueryRow(query, user.Email, user.HashedPassword, user.FirstName, user.LastName, user.Role, time.Now(), time.Now()).Scan(&user.ID)

	if err != nil {
		err := seelog.SeeLog.Errorf("Error creating user: %v", err.Error())
		if err != nil {
			return err
		}
		return err
	}
	return nil
}

// GetUsers retrieves a list of users from the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) GetUsers(page, pageSize int) ([]models.User, error) {
	query := `SELECT id, email, first_name, last_name, role, created_at, updated_at FROM users WHERE deleted_at IS NULL LIMIT $1 OFFSET $2`
	rows, err := repo.Db.Query(query, pageSize, (page-1)*pageSize)
	if err != nil {
		seelog.SeeLog.Errorf("Error executing the query: %s", err)
		return nil, err
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			seelog.SeeLog.Errorf("Error closing rows: %s", err)
		}
	}(rows)

	var users []models.User
	for rows.Next() {
		var u models.User
		err = rows.Scan(&u.ID, &u.Email, &u.FirstName, &u.LastName, &u.Role, &u.CreatedAt, &u.UpdatedAt)
		if err != nil {
			seelog.SeeLog.Errorf("Error scanning the row for user with ID %d: %s", u.ID, err)
			return nil, err
		}
		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		seelog.SeeLog.Errorf("Error processing rows: %s", err)
		return nil, err
	}

	return users, nil
}

// UpdateUser updates the information of a user in the user repository.
func (repo *UserRepositoryPostgres) UpdateUser(id string, user *models.User) error {
	query := `UPDATE users SET email=$1, hashed_password=$2, first_name=$3, last_name=$4, role=$5, updated_at=$6 WHERE id=$7 AND deleted_at IS NULL`
	_, err := repo.Db.Exec(query, user.Email, user.HashedPassword, user.FirstName, user.LastName, user.Role, time.Now(), id)
	return err
}

// DeleteUser deletes a user from the database.
func (repo *UserRepositoryPostgres) DeleteUser(user *models.User) error {
	query := `UPDATE users SET deleted_at=$1 WHERE id=$2`
	_, err := repo.Db.Exec(query, time.Now(), user.ID)
	return err
}

// GetUser retrieves a user with the given ID from the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) GetUser(id string) (*models.User, error) {
	user := &models.User{}
	query := `SELECT email, first_name, last_name, role FROM users WHERE id=$1`
	err := repo.Db.QueryRow(query, id).Scan(&user.Email, &user.FirstName, &user.LastName, &user.Role)
	return user, err
}

// insertUser is a method that inserts a new user record into the users table in the PostgresSQL database.
func (repo *UserRepositoryPostgres) insertUser(user *models.User) (sql.Result, error) {
	query := `INSERT INTO users (id, email, hashed_password, first_name, last_name, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7)`
	return repo.Db.Exec(query, user.ID, user.Email, user.HashedPassword, user.FirstName, user.LastName, time.Now(), time.Now())
}

// selectUsers retrieves a list of users from the database based on the specified page size and offset.
func (repo *UserRepositoryPostgres) selectUsers(pageSize, offset int) ([]models.User, error) {
	query := `SELECT id, email, first_name, last_name, created_at, updated_at FROM users WHERE deleted_at IS NULL LIMIT $1 OFFSET $2`
	rows, err := repo.Db.Query(query, pageSize, offset)
	if err != nil {
		seelog.SeeLog.Errorf("Error executing the query: %s", err)
		return nil, err
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			seelog.SeeLog.Errorf("Error closing rows: %s", err)
		}
	}(rows)

	var users []models.User
	for rows.Next() {
		var u models.User
		err = rows.Scan(&u.ID, &u.Email, &u.FirstName, &u.LastName, &u.CreatedAt, &u.UpdatedAt)
		if err != nil {
			seelog.SeeLog.Errorf("Error scanning the row for user with ID %d: %s", u.ID, err)
			return nil, err
		}
		users = append(users, u)
	}

	if err = rows.Err(); err != nil {
		seelog.SeeLog.Errorf("Error processing rows: %s", err)
		return nil, err
	}

	return users, nil
}

// updateUser updates the user's information in the database.
func (repo *UserRepositoryPostgres) updateUser(user *models.User) (sql.Result, error) {
	query := `UPDATE users SET email=$1, hashed_password=$2, first_name=$3, last_name=$4, updated_at=$5 WHERE id=$6 AND deleted_at IS NULL`
	return repo.Db.Exec(query, user.Email, user.HashedPassword, user.FirstName, user.LastName, time.Now(), user.ID)
}

// deleteUser updates the `deleted_at` field of a user in the database, marking it as deleted.
func (repo *UserRepositoryPostgres) deleteUser(id string) (sql.Result, error) {
	query := `UPDATE users SET deleted_at=$1 WHERE id=$2`
	return repo.Db.Exec(query, time.Now(), id)
}

// selectUser retrieves a user from the database based on the given ID.
func (repo *UserRepositoryPostgres) selectUser(id string) (*models.User, error) {
	user := &models.User{}
	query := `SELECT email, first_name, last_name FROM users WHERE id=$1`
	err := repo.Db.QueryRow(query, id).Scan(&user.Email, &user.FirstName, &user.LastName)
	return user, err
}

// GetUserByEmail is a method that retrieves a user from the UserRepositoryPostgres by their email.
func (repo *UserRepositoryPostgres) GetUserByEmail(email string) (*models.User, error) {
	var user models.User
	query := `SELECT * FROM users WHERE email = $1`

	err := repo.Db.QueryRow(query, email).Scan(&user.Email)

	if err != nil {
		if errors.Is(sql.ErrNoRows, err) {
			err := seelog.SeeLog.Errorf("No user found with email: %s", email)
			if err != nil {
				return nil, err
			}
			return nil, err
		}

		err := seelog.SeeLog.Errorf("Unexpected error retrieving user with email: %s, error: %v", email, err)
		if err != nil {
			return nil, err
		}
		return nil, err
	}

	return &user, nil
}

// CastVote is a method that inserts a new vote in the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) CastVote(userID, profileID, vote int) error {
	query := `INSERT INTO Votes (UserID, ProfileID, Vote, CreatedAt) VALUES ($1, $2, $3, $4)`
	_, err := repo.Db.Exec(query, userID, profileID, vote, time.Now())
	if err != nil {
		seelog.SeeLog.Errorf("Error creating vote: %v", err)
	}
	return err
}

// UpdateVote is a method that updates a user's vote in the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) UpdateVote(userID, profileID, vote int) error {
	query := `UPDATE Votes SET Vote = $1 WHERE UserID = $2 AND ProfileID = $3`
	_, err := repo.Db.Exec(query, vote, userID, profileID)
	if err != nil {
		seelog.SeeLog.Errorf("Error updating vote: %v", err)
	}
	return err
}

// DeleteVote is a method that deletes a user's vote in the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) DeleteVote(userID, profileID int) error {
	query := `DELETE FROM Votes WHERE UserID = $1 AND ProfileID = $2`
	_, err := repo.Db.Exec(query, userID, profileID)
	if err != nil {
		seelog.SeeLog.Errorf("Error deleting vote: %v", err)
	}
	return err
}

// GetVotes is a method that retrieves user votes from the UserRepositoryPostgres.
func (repo *UserRepositoryPostgres) GetVotes(userID int) ([]models.Vote, error) {
	query := `SELECT UserID, ProfileID, Vote, CreatedAt FROM Votes WHERE UserID = $1`
	rows, err := repo.Db.Query(query, userID)
	if err != nil {
		seelog.SeeLog.Errorf("Error retrieving votes: %v", err)
		return nil, err
	}
	defer rows.Close()

	var votes []models.Vote
	for rows.Next() {
		var v models.Vote
		err := rows.Scan(&v.UserID, &v.ProfileID, &v.Vote, &v.CreatedAt)
		if err != nil {
			seelog.SeeLog.Errorf("Error scanning vote: %v", err)
			return nil, err
		}
		votes = append(votes, v)
	}
	if err := rows.Err(); err != nil {
		seelog.SeeLog.Errorf("Error processing rows: %v", err)
		return nil, err
	}
	return votes, nil
}
