package main

import (
	"3-rest-api/internal/server"
	"database/sql"
	"errors"
	"fmt"
	"golang.org/x/net/context"
	"net/http"
	"os/signal"
	"syscall"
	"time"

	"3-rest-api/internal/cache"
	"3-rest-api/internal/config"
	"3-rest-api/internal/database"
	"3-rest-api/internal/handlers"
	"3-rest-api/internal/middlewares"
	"3-rest-api/internal/repository"
	"3-rest-api/internal/seelog"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

var ErrFailedToConnect = errors.New("failed to connect to the database after 10 attempts")

// It loads the configuration, establishes a database connection, and starts the HTTP server.
func main() {

	cache.Init()

	cfg, err := config.LoadConfig("configs/config.json")
	if err != nil {
		seelog.SeeLog.Errorf("Error loading config: %v", err)
		return
	}

	db, err := connectToDBWithRetry(&cfg)
	if err != nil {
		seelog.SeeLog.Errorf("Error connecting to DB: %v", err)
		return
	}

	defer seelog.Close()
	defer dbClose(db)

	userRepo := &repository.UserRepositoryPostgres{Db: db}
	userHandler := &handlers.UserHandler{Repo: userRepo}
	r := registerRoutes(userHandler, userRepo)

	srvCtx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	seelog.SeeLog.Info("Starting HTTP and gRPC servers...")

	// start HTTP server in a goroutine
	go func() {
		seelog.SeeLog.Info("HTTP server is starting...")
		<-srvCtx.Done() // wait for context done
		if err := http.ListenAndServe(cfg.Server.Port, r); err != nil {
			seelog.SeeLog.Errorf("Error starting HTTP server: %v", err)
		} else {
			seelog.SeeLog.Info("HTTP server is shutting down...")
		}
	}()

	// start gRPC Server in a goroutine
	go func() {
		seelog.SeeLog.Info("gRPC server is starting...")
		<-srvCtx.Done()
		if err := server.StartServer(); err != nil {
			seelog.SeeLog.Errorf("Error starting gRPC server: %v", err)
		} else {
			seelog.SeeLog.Info("gRPC server is shutting down...")
		}
	}()

	<-srvCtx.Done()
	seelog.SeeLog.Info("Both servers are shutting down, performing cleanup...")

	stop()

	seelog.SeeLog.Info("Servers have been stopped successfully. Exiting.")
}

func dbClose(db *sql.DB) {
	err := db.Close()
	if err != nil {
		seelog.SeeLog.Errorf("Error while closing the DB connection: %v", err)
	}
}

func connectToDBWithRetry(cfg *config.Config) (*sql.DB, error) {
	var lastError error

	for i := 0; i < 10; i++ {
		db, err := database.NewDatabase(cfg.Database.User, cfg.Database.Dbname, cfg.Database.Password, cfg.Database.Address)

		if err != nil {
			if lastError == nil || lastError.Error() != err.Error() {
				lastError = err
				seelog.SeeLog.Warnf("Unable to create new database, Attempt: %d, Error: %v", i, err)
			}
			time.Sleep(3 * time.Second)
			continue
		}

		err = db.Ping()
		if err != nil {
			if lastError == nil || lastError.Error() != err.Error() {
				lastError = err
				seelog.SeeLog.Warnf("Unable to ping database, Attempt: %d, Error: %v", i, err)
			}
			time.Sleep(3 * time.Second)
			continue
		}

		return db, nil
	}

	if lastError != nil {
		errorMessage := fmt.Sprintf("%s: %v", ErrFailedToConnect.Error(), lastError)
		seelog.SeeLog.Error(errorMessage)
		return nil, errors.New(errorMessage)
	}

	return nil, ErrFailedToConnect
}

// registerRoutes registers the routes for handling user-related operations.
func registerRoutes(userHandler *handlers.UserHandler, userRepo *repository.UserRepositoryPostgres) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/users", userHandler.GetUsersHandler).Methods(http.MethodGet)
	r.HandleFunc("/users", userHandler.CreateUserHandler).Methods(http.MethodPost)
	r.HandleFunc("/users/{id}", userHandler.GetUserHandler).Methods(http.MethodGet)

	secureRouter := r.PathPrefix("/users/{id}").Subrouter()
	secureRouter.Use(middlewares.AuthMiddleware(userRepo), middlewares.RoleBasedAccessControlHandler())
	secureRouter.HandleFunc("", userHandler.UpdateUserHandler).Methods(http.MethodPut)
	secureRouter.HandleFunc("", userHandler.DeleteUserHandler).Methods(http.MethodDelete)

	// routes for voting operations
	r.HandleFunc("/votes", userHandler.CastVoteHandler).Methods(http.MethodPost)
	r.HandleFunc("/votes", userHandler.UpdateVoteHandler).Methods(http.MethodPut)
	r.HandleFunc("/votes", userHandler.DeleteVoteHandler).Methods(http.MethodDelete)

	return r
}
