package seelog

import (
	"github.com/cihub/seelog"
)

var SeeLog seelog.LoggerInterface

func init() {
	var err error
	SeeLog, err = seelog.LoggerFromConfigAsBytes([]byte(`
		<seelog minlevel="debug">
			<outputs formatid="detail">
				<console/>
			</outputs>
			<formats>
				<format id="detail" format="%Date/%Time [%LEVEL] %Func: %Msg%n"/>
			</formats>
		</seelog>`))

	if err != nil {
		seelog.Errorf("Error initializing seelog: %v", err)
	}
}

func Close() {
	SeeLog.Flush()
}
