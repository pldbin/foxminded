CREATE TABLE votes (
                       user_id     INT NOT NULL,
                       profile_id  INT NOT NULL,
                       vote        INT NOT NULL,
                       created_at  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);