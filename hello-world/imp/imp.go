package imp

import "fmt"

func Imp() {
	arr := [5]int{5, 2, 14, 21, 20}
	min, max, minsum, maxsum := MaxMinSum(arr)
	fmt.Printf("minimumsum: %d, maximumsum: %d.", minsum-max, maxsum-min)
}
func MaxMinSum(arr [5]int) (min, max, minsum, maxsum int) {
	mi := arr[0]
	misum := 0
	ma := arr[0]
	masum := 0
	for _, numb := range arr {
		if numb < mi {
			mi = numb
		}
		misum += numb
		if numb > ma {
			ma = numb
		}
		masum += numb
	}
	return mi, ma, misum, masum
}
