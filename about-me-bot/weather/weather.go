package weather

import (
	"About_me_Bot/models"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// BuildAPIURL constructs the API URL for OpenWeatherMap
func BuildAPIURL(baseURL string, lat, lon float64, apiKey string) string {
	return fmt.Sprintf("%s/data/2.5/weather?lat=%f&lon=%f&appid=%s", baseURL, lat, lon, apiKey)
}

// DecodeWeatherResponse decodes weather data from JSON
func DecodeWeatherResponse(body []byte) (models.Weather, error) {
	var weather models.Weather
	err := json.Unmarshal(body, &weather)
	return weather, err
}

// validateArgs checks if the required arguments are supplied and within valid ranges
func validateArgs(apiKey string, lat, lon float64) error {
	if apiKey == "" {
		return fmt.Errorf("API key is required")
	}
	if lon < -180.0 || lon > 180.0 {
		return fmt.Errorf("longitude must be between -180 and 180")
	}
	if lat < -90.0 || lat > 90.0 {
		return fmt.Errorf("latitude must be between -90 and 90")
	}
	return nil
}

// GetWeather queries the OpenWeatherMap API for weather data
func GetWeather(baseURL string, lat, lon float64, apiKey string) (models.Weather, error) {
	err := validateArgs(apiKey, lat, lon)
	if err != nil {
		return models.Weather{}, err
	}

	// Build the API URL
	url := BuildAPIURL(baseURL, lat, lon, apiKey)

	// Send the HTTP request
	resp, err := http.Get(url)
	if err != nil {
		return models.Weather{}, err
	}
	defer resp.Body.Close()

	// Check if the API returned an error
	if resp.StatusCode >= 400 {
		return models.Weather{}, fmt.Errorf("API returned error status: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return models.Weather{}, err
	}

	// Decode the response body from JSON to a Weather struct
	weather, err := DecodeWeatherResponse(body)
	if err != nil {
		return models.Weather{}, err
	}

	// Check if the Weather slice is empty
	if len(weather.Weather) == 0 {
		return models.Weather{}, fmt.Errorf("No weather data received")
	}

	return weather, nil
}
