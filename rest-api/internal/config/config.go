package config

import (
	"encoding/json"
	"os"

	"3-rest-api/internal/seelog"
)

// DatabaseConfig represents the structure for the database configuration settings.
type DatabaseConfig struct {
	User     string `json:"user"`
	Dbname   string `json:"dbname"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

// ServerConfig represents the structure for the server configuration settings.
type ServerConfig struct {
	Port string `json:"port"`
}

// Config represents the structure for the configuration settings.
type Config struct {
	Database DatabaseConfig `json:"database"`
	Server   ServerConfig   `json:"server"`
}

// LoadConfig reads the contents of a JSON config file and unmarshal it into a Config struct.
func LoadConfig(file string) (Config, error) {
	var config Config
	configFile, err := os.Open(file)
	if err != nil {
		seelog.SeeLog.Errorf("Error opening config file %s: %v", file, err)
		return Config{}, err
	}
	defer func() {
		closeErr := configFile.Close()
		if closeErr != nil {
			seelog.SeeLog.Errorf("Error closing config file: %v", closeErr)
		}
	}()

	decoder := json.NewDecoder(configFile)
	err = decoder.Decode(&config)
	if err != nil {
		seelog.SeeLog.Errorf("Error decoding config for file %s: %v", file, err)
		return Config{}, err
	}

	return config, nil
}
