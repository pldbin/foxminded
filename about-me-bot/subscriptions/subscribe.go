package subscriptions

import (
	"context"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Item represents an item with chat ID, coordinates, and send time.
type Item struct {
	ChatID    int64   `bson:"chat_id"`
	Latitude  float64 `bson:"latitude"`
	Longitude float64 `bson:"longitude"`
}

// Model represents a model for accessing a MongoDB collection.
type Model struct {
	Collection *mongo.Collection
}

// NewModel creates a new instance of the Model struct with the specified client, database, and collection parameters.
func NewModel(client *mongo.Client, db, collection string) *Model {
	collectionHandle := client.Database(db).Collection(collection)
	model := &Model{
		Collection: collectionHandle,
	}
	return model
}

// Coordinatesur coordinatesUpsertRecord represents the coordinates for upserting a record.
type Coordinatesur struct {
	Lat float64
	Lon float64
}

// SaveLocation saves the given coordinates in the database.
func (m *Model) SaveLocation(chatID int64, coordinates Coordinatesur) error {
	filter := bson.M{"chat_id": chatID}
	update := bson.M{
		"$set": bson.M{
			"coordinates": coordinates,
		},
	}

	opt := options.Update().SetUpsert(true)
	_, err := m.Collection.UpdateOne(context.TODO(), filter, update, opt)
	if err != nil {
		log.Errorf("Failed to save location in database: %v", err)
		return err
	}
	return nil
}

// SaveSendTime updates the send time of a chat in the database
func (m *Model) SaveSendTime(chatID int64, sendHour int) error {
	filter := bson.M{"chat_id": chatID}
	update := bson.M{
		"$set": bson.M{
			"send_time": sendHour,
		},
	}

	opt := options.Update().SetUpsert(true)
	_, err := m.Collection.UpdateOne(context.TODO(), filter, update, opt)
	if err != nil {
		log.Errorf("Failed to save location in database: %v", err)
		return err
	}
	return nil
}

// GetUsersToNotify updates the send time of a chat in the database
func (m *Model) GetUsersToNotify(sendHour int) ([]Item, error) {
	ctx := context.TODO()
	projection := bson.D{
		{"coordinates", 1},
		{"chat_id", 1},
		{"_id", 0},
	}
	cursor, err := m.Collection.Find(ctx, bson.M{"send_time": sendHour}, options.Find().SetProjection(projection))
	if err != nil {
		return nil, err
	}

	defer cursor.Close(ctx)
	var users []Item
	for cursor.Next(ctx) {
		var user Item
		if err := cursor.Decode(&user); err != nil {
			log.Printf("Failed to decode user: %v", err)
			//return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}
