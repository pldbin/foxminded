package service

import (
	"context"
	"fmt"
	"strconv"

	pb "3-rest-api"
	"3-rest-api/internal/models"
	"3-rest-api/internal/repository"
	seelog "3-rest-api/internal/seelog"
)

type UserService struct {
	pb.UnimplementedUserServiceServer
	Repo repository.UserRepository
}

func NewUserService(repo repository.UserRepository) *UserService {
	return &UserService{
		Repo: repo,
	}
}

func UserToPb(user *models.User) *pb.User {
	return &pb.User{
		Id:        strconv.Itoa(user.ID),
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Role:      user.Role,
		Active:    user.Active,
	}
}

func (s *UserService) CreateUser(_ context.Context, req *pb.CreateUserRequest) (*pb.User, error) {
	user := &models.User{
		Email:     req.Email,
		Password:  req.Password,
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Role:      req.Role,
		Active:    req.Active,
	}

	if err := s.Repo.CreateUser(user); err != nil {
		seelog.SeeLog.Errorf("service: Failed to create user with email %s: %v", req.Email, err)
		return nil, fmt.Errorf("service: failed to create user: %v", err)
	}

	seelog.SeeLog.Infof("service: User with email %s created successfully", req.Email)
	return UserToPb(user), nil
}

func (s *UserService) GetUserByEmail(ctx context.Context, req *pb.GetUserByEmailRequest) (*pb.User, error) {
	user, err := s.Repo.GetUserByEmail(req.Email)
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to get user with email %s: %v", req.Email, err)
		return nil, fmt.Errorf("service: failed to get user by email: %v", err)
	}

	seelog.SeeLog.Infof("service: Successfully retrieved user with email %s", req.Email)
	return UserToPb(user), nil
}

func (s *UserService) GetUserByID(ctx context.Context, req *pb.GetUserByIDRequest) (*pb.User, error) {
	id, err := strconv.Atoi(req.Id)
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to parse user id %s: %v", req.Id, err)
		return nil, fmt.Errorf("service: failed to parse user id: %v", err)
	}

	user, err := s.Repo.GetUser(strconv.Itoa(id))
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to get user with id %s: %v", req.Id, err)
		return nil, fmt.Errorf("service: failed to get user by id: %v", err)
	}

	seelog.SeeLog.Infof("service: Successfully retrieved user with id %s", req.Id)
	return UserToPb(user), nil
}

func (s *UserService) GetUsers(ctx context.Context, req *pb.GetUsersRequest) (*pb.GetUsersResponse, error) {
	users, err := s.Repo.GetUsers(int(req.Page), int(req.PageSize))
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to get users: %v", err)
		return nil, fmt.Errorf("service: failed to get users: %v", err)
	}

	var usersPb []*pb.User
	for _, user := range users {
		usersPb = append(usersPb, UserToPb(&user))
	}

	seelog.SeeLog.Infof("service: Successfully retrieved %d users", len(usersPb))
	return &pb.GetUsersResponse{
		Users: usersPb,
	}, nil
}

func (s *UserService) UpdateUser(ctx context.Context, user *pb.User) (*pb.User, error) {
	id, err := strconv.Atoi(user.Id)
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to parse user id %s: %v", user.Id, err)
		return nil, fmt.Errorf("service: failed to parse user id: %v", err)
	}

	updatedUser := &models.User{
		ID:        id,
		Email:     user.Email,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Role:      user.Role,
		Active:    user.Active,
	}

	if err := s.Repo.UpdateUser(strconv.Itoa(id), updatedUser); err != nil {
		seelog.SeeLog.Errorf("service: Failed to update user with id %s: %v", user.Id, err)
		return nil, fmt.Errorf("service: failed to update user: %v", err)
	}

	seelog.SeeLog.Infof("service: User with id %s updated successfully", user.Id)
	return UserToPb(updatedUser), nil
}

func (s *UserService) DeleteUser(_ context.Context, req *pb.DeleteUserRequest) (*pb.User, error) {
	id, err := strconv.Atoi(req.Id)
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to parse user id %s: %v", req.Id, err)
		return nil, fmt.Errorf("service: failed to parse user id: %v", err)
	}

	user, err := s.Repo.GetUser(strconv.Itoa(id))
	if err != nil {
		seelog.SeeLog.Errorf("service: Failed to get user with id %s: %v", req.Id, err)
		return nil, fmt.Errorf("service: failed to get user: %v", err)
	}

	if err := s.Repo.DeleteUser(user); err != nil {
		seelog.SeeLog.Errorf("service: Failed to delete user with id %s: %v", req.Id, err)
		return nil, fmt.Errorf("service: failed to delete user: %v", err)
	}

	seelog.SeeLog.Infof("service: User with id %s deleted successfully", req.Id)
	return UserToPb(user), nil
}
