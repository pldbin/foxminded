package bot

import (
	"About_me_Bot/config"
	"About_me_Bot/logger"
	"About_me_Bot/subscriptions"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

// BotHandler is a struct that handles the Telegram bot functionality.
type BotHandler struct {
	Bot    *tgbotapi.BotAPI
	Logger logger.LogrusLogger
	Config *config.Config
	Model  *subscriptions.Model
}

// NewBotHandler is a constructor for BotHandler.
func NewBotHandler(cfg *config.Config, logger logger.LogrusLogger, model *subscriptions.Model) (*BotHandler, error) {
	bot, err := tgbotapi.NewBotAPI(cfg.Btoken)
	if err != nil {
		return nil, err
	}
	return &BotHandler{
		Bot:    bot,
		Logger: logger,
		Config: cfg,
		Model:  model,
	}, nil
}
