package handlers

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"3-rest-api/internal/cache"
	"3-rest-api/internal/models"
	"3-rest-api/internal/repository"
	"3-rest-api/internal/seelog"
	"github.com/go-redis/redis/v7"
	"github.com/gorilla/mux"
)

// UserInput validateUserInput validates the user input data and returns an error if validation fails.
type UserInput struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Role      string `json:"role"`
}

// Error variables
var (
	ErrInvalidUserID        = errors.New("invalid user ID")
	ErrInvalidUserData      = errors.New("invalid user data")
	ErrCreatingUser         = errors.New("error creating user")
	ErrGettingUser          = errors.New("error getting user")
	ErrUpdatingUser         = errors.New("error updating user")
	ErrDeletingUser         = errors.New("error deleting user")
	ErrGettingUsers         = errors.New("error getting users")
	ErrRequiredEmail        = errors.New("email is required")
	ErrPasswordRequirements = errors.New("password must be between 8 and 32 characters")
	ErrRequiredFirstName    = errors.New("FirstName is required")
	ErrRequiredLastName     = errors.New("LastName is required")
	ErrInvalidRole          = errors.New("invalid role, it should be 'admin', 'user', or 'guest'")
	ErrFailedToRetrieveID   = errors.New("error: Failed to retrieve identifier 'id' from query route variables. Check the URL and try again")
)

// minPasswordLength is the minimum allowed length for a password.
// maxPasswordLength is the maximum allowed length for a password.
const (
	minPasswordLength = 8
	maxPasswordLength = 32
)

// UserHandler is a struct that handles HTTP requests related to user operations. It contains a UserRepository to interact with the database.
type UserHandler struct {
	Repo repository.UserRepository
}

// getIdFromRequest extracts the "id" parameter from the given http.Request's mux.Vars and returns it as a string. If the "id" parameter is not found, it returns an error with the message
func getIdFromRequest(r *http.Request) (string, error) {
	id, ok := mux.Vars(r)["id"]
	if !ok {
		return "", ErrFailedToRetrieveID
	}
	return id, nil
}

// validateUser validates the fields of a User struct
func validateUserInput(userInput *UserInput) error {
	switch {
	case userInput.Email == "":
		return ErrRequiredEmail
	case len(userInput.Password) < minPasswordLength || len(userInput.Password) > maxPasswordLength:
		return ErrPasswordRequirements
	case userInput.FirstName == "":
		return ErrRequiredFirstName
	case userInput.LastName == "":
		return ErrRequiredLastName
	case userInput.Role != "admin" && userInput.Role != "user" && userInput.Role != "guest":
		return ErrInvalidRole
	default:
		return nil
	}
}

// encodeData encodes the provided data as JSON and writes it to the http.ResponseWriter.
func writeJSON(w http.ResponseWriter, data interface{}) {
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		seelog.SeeLog.Errorf("An error occurred while encoding the response data as JSON: %v", err)
	}
}

// CreateUserHandler handles the creation of a new user.
func (h *UserHandler) CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	userInput := UserInput{}
	err := json.NewDecoder(r.Body).Decode(&userInput)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to decode user input: %v", err.Error())
		http.Error(w, ErrInvalidUserData.Error(), http.StatusBadRequest)
		return
	}
	err = validateUserInput(&userInput)
	if err != nil {
		seelog.SeeLog.Errorf("User input validation failed: %v", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user := models.User{
		Email:     userInput.Email,
		Password:  userInput.Password,
		FirstName: userInput.FirstName,
		LastName:  userInput.LastName,
		Role:      userInput.Role,
	}
	err = h.Repo.CreateUser(&user)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to create user: %v", err.Error())
		http.Error(w, ErrCreatingUser.Error(), http.StatusInternalServerError)
		return
	}
	seelog.SeeLog.Info("User created successfully")
	w.WriteHeader(http.StatusInternalServerError)
}

// GetUserHandler is a method of the UserHandler struct that handles the GET request for retrieving user data.
func (h *UserHandler) GetUserHandler(w http.ResponseWriter, r *http.Request) {
	id, err := getIdFromRequest(r)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to get ID from request: %v", err.Error())
		http.Error(w, ErrInvalidUserID.Error(), http.StatusBadRequest)
		return
	}

	client := cache.GetClient()

	val, err := client.Get(fmt.Sprintf("user:%s", id)).Result()
	if err == redis.Nil {
		user, err := h.Repo.GetUser(id)
		if err != nil {
			seelog.SeeLog.Errorf("Failed to get user with ID %s: %v", id, err.Error())
			if errors.Is(err, sql.ErrNoRows) {
				w.WriteHeader(http.StatusNotFound)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}
			writeJSON(w, map[string]string{"error": ErrGettingUser.Error()})
			return
		}
		userData := map[string]string{
			"email":     user.Email,
			"firstName": user.FirstName,
			"lastName":  user.LastName,
			"role":      user.Role,
		}
		go client.Set(fmt.Sprintf("user:%s", id), userData, 1*time.Minute)
		writeJSON(w, userData)
		seelog.SeeLog.Info("User data fetched successfully from DB")
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, map[string]string{"error": "Error occurred fetching data from the cache"})
		seelog.SeeLog.Errorf("Error occurred fetching data from the cache: %v", err)
		return
	} else {
		userData := map[string]string{}
		err = json.Unmarshal([]byte(val), &userData)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			writeJSON(w, map[string]string{"error": "Error occurred while unmarshalling user data from cache"})
			seelog.SeeLog.Errorf("Error occurred while unmarshaling user data from cache: %v", err)
		} else {
			writeJSON(w, userData)
			seelog.SeeLog.Info("User data fetched successfully from cache")
		}
	}
}

// UpdateUserHandler handles the HTTP PUT request to update a user's information.
func (h *UserHandler) UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	id, err := getIdFromRequest(r)
	if err != nil {
		http.Error(w, ErrInvalidUserID.Error(), http.StatusBadRequest)
		return
	}

	user := models.User{}
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		writeJSON(w, map[string]string{"error": "Invalid user data."})
		return
	}

	err = h.Repo.UpdateUser(id, &user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, map[string]string{"error": "Error occurred whilst updating user data."})
		seelog.SeeLog.Errorf("Error occurred whilst updating user with ID %s: %v", id, err)
		return
	}

	client := cache.GetClient()
	go client.Set(fmt.Sprintf("user:%s", id), user, 1*time.Minute)

	writeJSON(w, map[string]string{"status": "User updated and cached successfully."})
	seelog.SeeLog.Info("User updated and cached successfully.")
}

// GetUsersHandler retrieves a list of users based on the provided page and pageSize query parameters.
func (h *UserHandler) GetUsersHandler(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	if page < 1 {
		page = 1
	}
	pageSize, _ := strconv.Atoi(r.URL.Query().Get("pageSize"))
	if pageSize < 1 || pageSize > 50 {
		pageSize = 10
	}

	client := cache.GetClient()

	val, err := client.Get(fmt.Sprintf("users:%d:%d", page, pageSize)).Result()
	if err == redis.Nil {
		users, err := h.Repo.GetUsers(page, pageSize)
		if err != nil {
			seelog.SeeLog.Errorf("Failed to get users: %v", err.Error())
			http.Error(w, ErrGettingUsers.Error(), http.StatusInternalServerError)
			return
		}
		go client.Set(fmt.Sprintf("users:%d:%d", page, pageSize), users, 1*time.Minute)
		writeJSON(w, users)
		seelog.SeeLog.Info("Users data fetched successfully from DB")
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, map[string]string{"error": "Error occurred fetching data from the cache"})
		seelog.SeeLog.Errorf("Error occurred fetching data from the cache: %v", err)
	} else {
		var users []models.User
		err = json.Unmarshal([]byte(val), &users)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			writeJSON(w, map[string]string{"error": "Error occurred while unmarshaling users data from cache"})
			seelog.SeeLog.Errorf("Error occurred while unmarshaling users data from cache: %v", err)
		} else {
			writeJSON(w, users)
			seelog.SeeLog.Info("Users data fetched successfully from cache")
		}
	}
}

// DeleteUserHandler deletes a user based on the ID provided in the request URL.
func (h *UserHandler) DeleteUserHandler(w http.ResponseWriter, r *http.Request) {
	id, err := getIdFromRequest(r)
	if err != nil {
		http.Error(w, ErrInvalidUserID.Error(), http.StatusBadRequest)
		return
	}

	// Get user from the repository
	user, err := h.Repo.GetUser(id)
	if err != nil {
		// Handle error
		http.Error(w, "User not found", http.StatusNotFound)
		return
	}

	// Try deleting user
	err = h.Repo.DeleteUser(user)
	if err != nil {
		// Handle error
		w.WriteHeader(http.StatusInternalServerError)
		writeJSON(w, map[string]string{"error": "Error occurred while deleting user."})
		seelog.SeeLog.Errorf("Error occurred while deleting user with ID %s: %v", id, err)
		return
	}

	// Get Cache Client
	client := cache.GetClient()
	go client.Del(fmt.Sprintf("user:%s", id))
	writeJSON(w, map[string]string{"status": "User deleted successfully and removed from cache."})
	seelog.SeeLog.Info("User with ID %s deleted successfully and removed from cache.", id)
}

// CastVoteHandler handles the POST request to cast a vote.
func (h *UserHandler) CastVoteHandler(w http.ResponseWriter, r *http.Request) {
	vote := models.Vote{}
	err := json.NewDecoder(r.Body).Decode(&vote)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to decode vote data: %v", err.Error())
		http.Error(w, "Invalid vote data", http.StatusBadRequest)
		return
	}

	// Check if the user is trying to vote for themselves
	if vote.UserID == vote.ProfileID {
		seelog.SeeLog.Error("User cannot vote for themselves")
		http.Error(w, "You cannot vote for yourself", http.StatusForbidden)
		return
	}

	// Check if the user has already voted for this profile
	existingVotes, _ := h.Repo.GetVotes(vote.UserID)
	for _, existingVote := range existingVotes {
		if existingVote.ProfileID == vote.ProfileID {
			seelog.SeeLog.Error("User has already voted for this profile")
			http.Error(w, "You can only vote once per profile", http.StatusForbidden)
			return
		}

		// Check if the user is trying to vote again within the last hour
		if time.Since(existingVote.CreatedAt).Hours() < 1 {
			seelog.SeeLog.Error("User can only vote once per hour")
			http.Error(w, "You can only vote once per hour", http.StatusForbidden)
			return
		}
	}

	err = h.Repo.CastVote(vote.UserID, vote.ProfileID, vote.Vote)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to cast vote: %v", err.Error())
		http.Error(w, "Error casting vote", http.StatusInternalServerError)
		return
	}
	seelog.SeeLog.Info("Vote successfully cast")
	w.WriteHeader(http.StatusOK)
}

// UpdateVoteHandler handles the PUT request to update a vote.
func (h *UserHandler) UpdateVoteHandler(w http.ResponseWriter, r *http.Request) {
	vote := models.Vote{}
	err := json.NewDecoder(r.Body).Decode(&vote)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to decode vote data: %v", err.Error())
		http.Error(w, "Invalid vote data", http.StatusBadRequest)
		return
	}
	err = h.Repo.UpdateVote(vote.UserID, vote.ProfileID, vote.Vote)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to update vote: %v", err.Error())
		http.Error(w, "Error updating vote", http.StatusInternalServerError)
		return
	}
	seelog.SeeLog.Info("Vote successfully updated")
	w.WriteHeader(http.StatusOK)
}

// DeleteVoteHandler handles the DELETE request to delete a vote.
func (h *UserHandler) DeleteVoteHandler(w http.ResponseWriter, r *http.Request) {
	vote := models.Vote{}
	err := json.NewDecoder(r.Body).Decode(&vote)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to decode vote data: %v", err.Error())
		http.Error(w, "Invalid vote data", http.StatusBadRequest)
		return
	}
	err = h.Repo.DeleteVote(vote.UserID, vote.ProfileID)
	if err != nil {
		seelog.SeeLog.Errorf("Failed to delete vote: %v", err.Error())
		http.Error(w, "Error deleting vote", http.StatusInternalServerError)
		return
	}
	seelog.SeeLog.Info("Vote successfully deleted")
	w.WriteHeader(http.StatusOK)
}
