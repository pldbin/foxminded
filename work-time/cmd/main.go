package main

import (
	"1.3-work-time/app"
	"fmt"
	"log"
)

func main() {
	result, result2, err := app.WorkTime("03:04:05PM")
	if err != nil {
		log.Fatalf("Error: '%v'\n'", err)
	}
	fmt.Printf("result: '%v'\n", result, result2)
}
