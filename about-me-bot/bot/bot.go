package bot

import (
	"About_me_Bot/subscriptions"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	log "github.com/sirupsen/logrus"
	"strconv"
	"time"
)

const (
	locationText = "We got your location! Now I'll show you the weather."
)

// Startbot is a method that starts the bot and handles incoming updates from Telegram.
func (bh *BotHandler) Startbot() error {

	log.Printf("Authorized on account %s", bh.Bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bh.Bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}
		bh.handleUserCommand(update)
	}
	return nil
}

// MenuCommand represents a command for a menu or menu item.
type MenuCommand string

const (
	askСoordinatesCommand MenuCommand = "subscriptions"
	unsubscribe           MenuCommand = "unsubscribe"
	askTimeCommand        MenuCommand = "askTime"
	exitCommand           MenuCommand = "exit"
)

// handleUserCommand is a method that handles user commands received from Telegram.
func (bh *BotHandler) handleUserCommand(update tgbotapi.Update) {
	if update.Message == nil {
		return
	}
	if update.Message.IsCommand() {
		bh.handleCommand(update)
		return
	}
	if update.Message.Location != nil {
		bh.handleLocation(update)
		return
	}
	if _, err := strconv.Atoi(update.Message.Text); err == nil {
		bh.handleSubscriptionTime(update)
		return
	}
	bh.SendMessage(update.Message.Chat.ID, "Unknow input type. Please try again.")
}

// handleCommand is a method that handles user commands received from Telegram.
func (bh *BotHandler) handleCommand(update tgbotapi.Update) {
	switch MenuCommand(update.Message.Command()) {
	case askСoordinatesCommand:
		bh.askСoordinates(update.Message.Chat.ID)
	case unsubscribe:
		bh.unsubscriber(update.Message.Chat.ID)
	case askTimeCommand:
		bh.askTime(update.Message.Chat.ID)
	case exitCommand:
		bh.exit(update.Message.Chat.ID)
	default:
		bh.SendMessage(update.Message.Chat.ID, "Unknown command. Please try again.")
	}
}

// handleWeatherByLocation is used to handle weather information by location
func (bh *BotHandler) handleLocation(update tgbotapi.Update) {
	// Processing and handling the coordinates here
	bh.handleWeatherByLocation(update.Message.Chat.ID, update.Message.Location)

	// Save coordinates to DB
	chatID := update.Message.Chat.ID

	err := bh.Model.SaveLocation(chatID, subscriptions.Coordinatesur{
		Lat: update.Message.Location.Latitude,
		Lon: update.Message.Location.Longitude,
	})
	if err != nil {
		bh.SendMessage(update.Message.Chat.ID, "Failed to update location in database.")
		return
	}
	// Send a confirmation message if you want
	bh.SendMessage(update.Message.Chat.ID, "Please enter a time (hour) for the subscription.")
	bh.handleSubscriptionTime(update)

}

// sendMessage sends a message to the specified chat ID with the given text.
func (bh *BotHandler) SendMessage(chatID int64, text string) error {
	msg := tgbotapi.NewMessage(chatID, text)
	_, err := bh.Bot.Send(msg)
	if err != nil {
		bh.Logger.Errorf("Error: '%v'\n", err)
		return err
	}
	return nil
}

// handleText is a private method for handling text messages from the user.
func (bh *BotHandler) handleSubscriptionTime(update tgbotapi.Update) {
	// User input is contained in update.Message.Text
	userInput := update.Message.Text

	// Parse the user input to an integer
	subscriptionTime, err := strconv.Atoi(userInput)

	if err != nil {
		// If conversion fails, send a message and return
		bh.SendMessage(update.Message.Chat.ID, "Please enter a valid hour between 0 and 23.")
		return
	}

	if subscriptionTime < 0 || subscriptionTime > 23 {
		// If hour is not valid, send a message and return
		bh.SendMessage(update.Message.Chat.ID, "Please enter a valid hour between 0 and 23.")
		return
	}

	zeroTime := time.Date(0, 0, 0, subscriptionTime, 0, 0, 0, time.UTC)
	// Save subscription time to DB
	chatID := update.Message.Chat.ID
	err = bh.Model.SaveSendTime(chatID, zeroTime.Hour())

	if err != nil {
		// Handle any errors during saving
		bh.SendMessage(update.Message.Chat.ID, "Failed to update subscription time in database.")
		return
	}

	bh.SendMessage(update.Message.Chat.ID, "Hour successfully set!")
}
