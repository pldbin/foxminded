package app

import (
	"About_me_Bot/bot"
	"About_me_Bot/config"
	"About_me_Bot/logger"
	"About_me_Bot/notification"
	"About_me_Bot/subscriptions"
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

// Run is the main function of the application. It initializes the configuration, logger, and starts the bot.
// It also starts a goroutine that sends weather forecast messages to users every hour.
// If an error occurs during the execution, it is returned to the caller.
func Run() error {
	// Load the configuration from environment variables
	cfg, err := config.NewConfig()
	if err != nil {
		logger.DebugError("config.NewConfig() failed. Error: ", err)
		return err
	}

	// Initialize the logger
	lgr, err := logger.New(&logger.Config{
		LogLevel:    cfg.LogLevel,
		LogServer:   cfg.LogServer,
		ServiceName: cfg.ServiceName,
	})
	if err != nil {
		lgr.Warnf("error in func Run: It initializes the configuration, logger, and starts the bot. %v", err)
		return err
	}

	// Create a context with timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Connect to MongoDB
	uri := cfg.MongoDBURI
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	if err != nil {
		lgr.Errorf("Failed to initialize mongo client: %v", err)
		return err
	}

	// Initialize the subscriptions model
	db, collection := "Sub", "TelegramBot"
	mdl := subscriptions.NewModel(client, db, collection)

	// Initialize the bot handler
	bh, err := bot.NewBotHandler(cfg, *lgr, mdl)
	if err != nil {
		log.Fatalf("Error creating BotHandler: %v", err)
	}

	// Start a goroutine that sends weather forecast messages to users every hour
	go func() {
		ticker := time.NewTicker(time.Hour)
		defer ticker.Stop()
		for t := range ticker.C {
			err := notification.NotifyUsers(bh, mdl, lgr, t)
			if err != nil {
				log.Printf("Failed to send weather forecast: %v", err)
			}
		}
	}()

	// Start the bot
	if err := bh.Startbot(); err != nil {
		log.Fatalf("Failed to start the bot. Error: %v", err)
	}

	return nil
}
