package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

// askСoordinates requests user coordinates by sending a message to the chat.
func (bh *BotHandler) askСoordinates(chatID int64) {
	msg := tgbotapi.NewMessage(chatID, "Please send me your location.")
	msg.ReplyMarkup = tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButtonLocation("Send location"),
		),
	)
	bh.Bot.Send(msg)
}
