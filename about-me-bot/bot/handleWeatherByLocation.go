package bot

import (
	"About_me_Bot/weather"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

// handleWeatherByLocation handles the weather request based on the user's location
func (bh *BotHandler) handleWeatherByLocation(chatID int64, location *tgbotapi.Location) {
	if location == nil {
		bh.Logger.Warnf("Location is nil")
		return
	}
	currentWeather, err := weather.GetWeather(bh.Config.OpenWeatherMapAPIURL, location.Latitude, location.Longitude, bh.Config.WeatherAPI)
	if err != nil {
		bh.Logger.Errorf("We had an error getting the weather data, we are working on it.: '%v'\n", err)
		return
	}

	description := currentWeather.Weather[0].Description
	temp := fmt.Sprintf("%.1f", currentWeather.Main.Temp-273) + "\u2103"
	feelsLikeTemp := fmt.Sprintf("%.1f", currentWeather.Main.FeelsLike-273) + "\u2103"
	windSpeed := fmt.Sprintf("%.1f", currentWeather.Wind.Speed) + " м/с"

	weatherAnswer := fmt.Sprintf("\n A̲t̲Y̲o̲u̲r̲A̲d̲d̲r̲e̲s̲s̲\nNow: %s,\nTemperature: %s,\nFeeling: %s,\nWind: %s.",
		description, temp, feelsLikeTemp, windSpeed)

	msg := tgbotapi.NewMessage(chatID, weatherAnswer)
	_, err = bh.Bot.Send(msg)
	if err != nil {
		bh.Logger.Errorf("Error: '%v'\n", err)
	}
	keyboard := tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("/start"),
		),
	)
	msg.ReplyMarkup = keyboard
	bh.SendMessage(chatID, locationText)
}
