package main

import (
	"About_me_Bot/app"
	"log"
)

func main() {
	// Run the bot
	if err := app.Run(); err != nil {
		log.Fatalf("app.Run() failed with error: %v", err)
	}

}
