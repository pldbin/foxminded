package models

import "time"

// User represents a user in the system.
type User struct {
	ID             int       `json:"id"`
	Email          string    `json:"email"`
	Password       string    `json:"password"`
	HashedPassword string    `json:"-"`
	FirstName      string    `json:"first_name"`
	LastName       string    `json:"last_name"`
	Role           string    `json:"role"`
	Active         bool      `json:"active"`
	Rating         int       `json:"rating"`
	CreatedAt      time.Time `json:"created_at"`
	UpdatedAt      time.Time `json:"updated_at"`
	DeletedAt      time.Time `json:"deleted_at"`
}

// Vote represents a vote in the system.
type Vote struct {
	UserID    int       `json:"user_id"`
	ProfileID int       `json:"profile_id"`
	Vote      int       `json:"vote"`
	CreatedAt time.Time `json:"created_at"`
}
