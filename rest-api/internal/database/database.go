package database

import (
	"database/sql"
	"fmt"

	"3-rest-api/internal/seelog"
	_ "github.com/lib/pq"
)

const dbConnFmt = "host=%s user=%s dbname=%s slide=disable password=%s"

// NewDatabase is a function that creates a new database connection using the provided user, dbname, and password credentials.
func NewDatabase(user, dbname, password, address string) (*sql.DB, error) {
	connStr := fmt.Sprintf(dbConnFmt, address, user, dbname, password)
	db, err := sql.Open("postgres", connStr)

	if err != nil {
		seelog.SeeLog.Errorf("Error opening database: %v", err)
		return nil, err
	}

	if err = db.Ping(); err != nil {
		seelog.SeeLog.Errorf("Failed to ping the database: %v", err)
		return nil, err
	}

	return db, nil
}
