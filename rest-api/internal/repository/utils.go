package repository

import (
	"3-rest-api/internal/seelog"
	"golang.org/x/crypto/bcrypt"
)

// BcryptHashPassword takes in a password string and returns its bcrypt hash as a string.
func BcryptHashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		seelog.SeeLog.Errorf("An error occurred while hashing the password: %v", err)
		return "", err
	}
	return string(hash), nil
}
