package server

import (
	"net"

	pb "3-rest-api"
	"3-rest-api/internal/config"
	"3-rest-api/internal/database"
	"3-rest-api/internal/repository"
	seelog "3-rest-api/internal/seelog"
	service "3-rest-api/internal/services"

	"google.golang.org/grpc"
)

func StartServer() error {
	conf, err := config.LoadConfig("config.json")
	if err != nil {
		seelog.SeeLog.Errorf("StartServer: Failed to load config: %v", err)
		return err
	}

	listener, err := net.Listen("tcp", conf.Server.Port)
	if err != nil {
		seelog.SeeLog.Errorf("StartServer: Failed to listen: %v", err)
		return err
	}

	// Connect to DB
	db, err := database.NewDatabase(conf.Database.User, conf.Database.Dbname, conf.Database.Password, conf.Database.Address)
	if err != nil {
		seelog.SeeLog.Errorf("StartServer: Failed to connect to database : %v", err)
		return err
	}

	// Create UserRepository
	repo := repository.NewUserRepository(db)

	// Create UserService
	srv := service.NewUserService(repo)

	// Create gRPC server
	grpcServer := grpc.NewServer()

	// Register UserService on gRPC server
	pb.RegisterUserServiceServer(grpcServer, srv)

	// Start gRPC server
	seelog.SeeLog.Infof("StartServer: Starting server on port %s...\n", conf.Server.Port)
	err = grpcServer.Serve(listener)
	if err != nil {
		seelog.SeeLog.Errorf("StartServer: Failed to serve gRPC server: %v", err)
	}
	defer seelog.Close()

	return err
}
