package middlewares

import (
	"net/http"
	"strconv"

	"3-rest-api/internal/repository"
	"3-rest-api/internal/seelog"
	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

// AuthMiddleware performs authentication before executing the next handler.
func AuthMiddleware(userRepo *repository.UserRepositoryPostgres) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			// Get username (email) and password from header using Basic Authentication
			userEmail, password, ok := r.BasicAuth()

			// Return 400 status if Basic Auth credentials are not found
			if !ok {
				err := seelog.SeeLog.Errorf("Invalid request, Basic Auth credentials are not found")
				if err != nil {
					return
				}
				http.Error(w, "Invalid request", http.StatusBadRequest)
				return
			}

			user, err := userRepo.GetUserByEmail(userEmail)

			// Return 500 status if there's an error when getting user
			if err != nil {
				err := seelog.SeeLog.Errorf("Error when getting user: %v", err)
				if err != nil {
					return
				}
				http.Error(w, "500 status error when getting user", http.StatusInternalServerError)
				return
			}

			// Return 401 status if user not found
			if user == nil {
				err := seelog.SeeLog.Errorf("User not found.")
				if err != nil {
					return
				}
				http.Error(w, "401 status if user not found Unauthorized", http.StatusUnauthorized)
				return
			}

			// Validate if hashed password matches the one in DB; if not, return 401 error
			err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
			if err != nil {
				err := seelog.SeeLog.Errorf("Error, validate if hashed password matches the one in DB: %v", err)
				if err != nil {
					return
				}
				http.Error(w, "Error, validate if hashed password matches the one in DB", http.StatusUnauthorized)
				return
			}

			// Check if user is the owner of the requested resource, if not return 403 error
			// 'id' in the route must be the user's ID
			pathId := mux.Vars(r)["id"]
			if pathId != strconv.Itoa(user.ID) {
				seelog.SeeLog.Errorf("Received Forbidden request.")
				http.Error(w, "Forbidden", http.StatusForbidden)
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}
